<?php

    namespace EmzTax;

    class EmzTax extends \Shopware\Components\Plugin
    {

        public static function getSubscribedEvents()
        {
            return [
                'Enlight_Controller_Action_PostDispatchSecure' => 'registerTemplateDir',
            ];
        }

        public function registerTemplateDir()
        {
            $this->container->get('Template')->addTemplateDir(
                $this->getPath() . '/Resources/Views/'
            );
        }
    }
